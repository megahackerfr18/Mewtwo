# Starts up all the services using screen all in one go.
# Currently only supports Unix based systems.

import sys, os, time

isProduction = '--prod' in sys.argv

os.chdir('startup/unix')

if not isProduction:
    os.system('screen -dmS MongoDB ./run_mongo.sh')

os.system('screen -dmS OTP ./run_otp_server.sh')
os.system('screen -dmS UberDOG ./run_server.sh')

# Wait until the UberDOG server starts.
time.sleep(5)

os.system('cd ../.. && screen -dmS Districts python3 -m DistrictStarter')

if isProduction:
    os.system('screen -dmS Stunnel ./run_stunnel.sh')
    os.system('screen -dmS Endpoints ./run_endpoint_manager.sh')

    os.system('cd ../../../discord-status-bot && python3 -m Starter')
