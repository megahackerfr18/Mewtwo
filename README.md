Mewtwo
============

Mewtwo is a set of game server code for various versions of Disney's Toontown Online.

## Panda3D
[Windows SDK (x86_64)](https://download.alt.sunrise.games/binaries/Panda3D-1.11.0-py3.11-x64_otp.exe)

[Linux SDK (x86_64)](https://download.alt.sunrise.games/binaries/linux/panda3d1.11_1.11.0_amd64_otp.deb)

## Getting Started
* `git clone https://gitlab.com/sunrisemmos/Mewtwo && cd Mewtwo`
* `python -m pip install -r requirements.txt`
* `cd startup/win32`
* `run_otp_server.bat`
* `run_server.bat`
* `run_ai.bat`

## Components
* [OTP Server](https://github.com/LittleToonCat/OtpGo)
* [Panda3D](https://github.com/rocketprogrammer/panda3d/tree/otp-with-decompile)
* [Anesidora](https://github.com/satire6/Anesidora)
* [DNA Parser](https://github.com/rocketprogrammer/panda3d/tree/otp-with-decompile/panda/src/dna)
