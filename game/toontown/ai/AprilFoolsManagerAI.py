##############################################
# Class: LowGravManagerAI
# This class handles April Fools changes
##############################################

from game.toontown.ai import HolidayBaseAI
from game.toontown.ai import CostumeManagerAI
from game.toontown.toonbase import ToontownGlobals
from direct.showbase import DirectObject
from game.toontown.toonbase import TTLocalizer
from direct.directnotify import DirectNotifyGlobal

class AprilFoolsManagerAI(CostumeManagerAI.CostumeManagerAI):
    notify = DirectNotifyGlobal.directNotify.newCategory('AprilFoolsManagerAI')

    def __init__(self, air, holidayId):
        CostumeManagerAI.CostumeManagerAI.__init__(self, air, holidayId)

    # Overridden function
    def start(self):
        CostumeManagerAI.CostumeManagerAI.start(self)

        estateManager = simbase.air.doFind("EstateManagerAI.EstateManagerAI")
        if estateManager != None:
            estateManager.startAprilFools()

    # Overridden function
    def stop(self):
        CostumeManagerAI.CostumeManagerAI.stop(self)

        estateManager = simbase.air.doFind("EstateManagerAI.EstateManagerAI")
        if estateManager != None:
            estateManager.stopAprilFools()
