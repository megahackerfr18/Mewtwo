from direct.directnotify import DirectNotifyGlobal
from game.toontown.ai import HolidayBaseAI
from game.toontown.ai import PropBuffHolidayAI
from game.toontown.ai import DistributedPhaseEventMgrAI
from game.toontown.toonbase import ToontownGlobals

class TrashcanBuffHolidayAI(PropBuffHolidayAI.PropBuffHolidayAI):

    notify = DirectNotifyGlobal.directNotify.newCategory(
        'TrashcanBuffHolidayAI')

    PostName = 'TrashcanBuffHoliday'

    def __init__(self, air, holidayId, startAndEndTimes, phaseDates):
        PropBuffHolidayAI.PropBuffHolidayAI.__init__(self, air, holidayId, startAndEndTimes, phaseDates)
        
