from direct.directnotify.DirectNotifyGlobal import directNotify
from direct.distributed.DistributedObjectGlobalAI import DistributedObjectGlobalAI

class DistributedInGameNewsMgrAI(DistributedObjectGlobalAI):
    notify = directNotify.newCategory('DistributedInGameNewsMgrAI')

    doNotListenToChannel = 1

    def __init__(self, air):
        DistributedObjectGlobalAI.__init__(self, air)

        # I think this is from the verbose logs I've gotten a while
        # back in 2013,  I've since lost said logs, but
        # This is in my other OTP implementations (including my old
        # TTRev server emulator), sooo this is as accurate as it
        # could get.  ~Little Cat
        self.latestIssueStr = '2013-08-22 23:49:46'

    def setLatestIssueStr(self, latestIssueStr):
        self.latestIssueStr = latestIssueStr

    def d_setLatestIssueStr(self, latestIssueStr):
        self.sendUpdate('setLatestIssueStr', [latestIssueStr])

    def b_setLatestIssueStr(self, latestIssueStr):
        self.setLatestIssueStr(latestIssueStr)
        self.d_setLatestIssueStr(latestIssueStr)

    def getLatestIssueStr(self):
        return self.latestIssueStr

    def inGameNewsMgrAIStartingUp(self, todo0, todo1):
        pass

    def newIssueUDtoAI(self, todo0):
        pass
