class ServerGlobals:
    FINAL_TOONTOWN = 1
    TEST_TOONTOWN_2012 = 2
    FINAL_TOONTOWN_JP = 3
    TOONTOWN_BR = 4

    serverToName = {
        FINAL_TOONTOWN: 'Final Toontown',
        TEST_TOONTOWN_2012: 'Test Toontown 2012',
        FINAL_TOONTOWN_JP: 'Final Toontown Japan',
        TOONTOWN_BR: 'Final Toontown Brazil'
    }
