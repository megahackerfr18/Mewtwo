# Internal
dc-file config/dclass/toon.dc
dc-file config/dclass/otp.dc

# Environment
server-type dev

# Features
want-neighborhoods true
want-randomized-hats false
want-cogdominiums true
want-code-redemption true
want-discord-integration true
want-ds-anydbm false

# Code Redemption
max-code-redemption-attempts 5

# Backups
buildings-server-data-folder backups/buildings
store-server-data-folder backups/store
secret-friend-storage backups/friendCodes/secrets.json
bingo-server-data-folder backups/bingo

# Whitelist
whitelist-url http://download.sunrise.games/launcher/
whitelist-stage-dir game/whitelist
whitelist-over-http false

# Resources
model-path game/resources
