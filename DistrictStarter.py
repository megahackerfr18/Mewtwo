import os, subprocess, sys, random

NUM_DISTRICTS = 2

districtNames = [
    'Boingy Acres',
    'Boingy Bay',
    'Boingy Summit',
    'Boingyboro',
    'Bouncy Summit',
    'Crazy Grove',
    'Crazy Hills',
    'Crazyham',
    'Funnyfield',
    'Giggly Bay',
    'Giggly Grove',
    'Giggly Hills',
    'Giggly Point',
    'Gigglyfield',
    'Gigglyham',
    'Goofy Valley',
    'Goofyport',
    'Kooky Grove',
    'Kookyboro',
    'Loopy Harbor',
    'Nutty Hills',
    'Nutty River',
    'Nutty Summit',
    'Nuttyville',
    'Nuttywood',
    'Silly Rapids',
    'Silly Valley',
    'Sillyham',
    'Toon Valley',
    'Zany Acres'
]

cutDistrictNames = random.sample(districtNames, NUM_DISTRICTS)

# if districtNames[21] not in cutDistrictNames:
    # Add Nutty River to the district list.
    # cutDistrictNames.append(districtNames[21])

startingNum = 200000000
minObjIdBase = 200100000
maxObjIdBase = 200149999

isWindows = sys.platform == 'win32'
isLinux = sys.platform == 'linux'

if isWindows:
    os.chdir('startup/win32')
elif isLinux:
    os.chdir('startup/unix')

for index, elem in enumerate(cutDistrictNames):
    subprocess.shell = True

    districtName = str(cutDistrictNames[index])

    os.environ['DISTRICT_NAME'] = districtName
    os.environ['BASE_CHANNEL'] = str(startingNum)
    os.environ['MIN_OBJ_ID'] = str(minObjIdBase)
    os.environ['MAX_OBJ_ID'] = str(maxObjIdBase)

    if isWindows:
        os.system('start cmd /c districtStarter.bat')
    elif isLinux:
        os.system(f'screen -dmS "{districtName}" ./districtStarter.sh')

    startingNum = startingNum + 1000000
    minObjIdBase = minObjIdBase + 1000000
    maxObjIdBase = maxObjIdBase + 1000000
