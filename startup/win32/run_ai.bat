@echo off
cd ../..

title Sillyville

set /P PYTHON_PATH=<PYTHON_PATH

:main
%PYTHON_PATH% -m game.toontown.ai.AIStart config/Config.prc
pause
goto :main
